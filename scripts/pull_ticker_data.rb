#!/usr/bin/env ruby

require 'active_record'
require 'date'
require 'open-uri'
require 'optparse'

class Ticker < ActiveRecord::Base
  establish_connection(
    :adapter  => "sqlite3",
    :host     => "localhost",
    :database => "db/development.sqlite3",
  )
end

$options = {};

$optParse = OptionParser.new { |opts|
  opts.banner = "Usage: pull_ticker_data.rb [options]";

  opts.on('-q', '--quote [SYMBOL[, ...]]', Array, "Pull data for SYMBOLS; if no EXCHANGE given, will automatically choose the EXCHANGE.") { |sym|
    $options[:quotes] = sym;
    puts "ARGV: QUOTES: #{$options[:quotes]}" if $options[:debug];
  }

  opts.on('-x', '--exchange [EXCHANGE]', "EXCHANGE to pull data from (NYSE, NASDAQ, etc.)") { |exchange|
    $options[:exchange] = exchange;
    puts "ARGV: EXCHANGE: #{$options[:exchange]}" if $options[:debug];
  }

  opts.on('-i', '--interval [SECONDS]', Integer, "Price pull interval in SECONDS.") { |interval|
    $options[:interval] = interval;
    puts "ARGV: INTERVAL: #{$options[:interval]}" if $options[:debug];
  }

  opts.on('-p', '--period [{AMOUNT}d,M,Y]', "Length of time to pull data from; '1d' is 1 day, '1M' is 1 month, etc.") { |period|
    $options[:period] = period;
    puts "ARGV: PERIOD: #{$options[:period]}" if $options[:debug];
  }

  opts.on('-v', '--verbose', "Outputs verbosely.") {
    $options[:verbose] = true;
  }

  opts.on('-d', '--debug', "Outputs debug statements.") {
    $options[:debug] = true;
  }

  opts.on('-h', '--help', "Shows this help text.") {
    puts opts;
    exit;
  }

}

$optParse.parse!;

if $options[:quotes].nil?
  puts "Missing required argument -q, --quote.\n";
  puts "======================================\n\n";
  puts $optParse;
  exit;
end

# FINISH OPTION PARSING
# =====================

$interval     = $options[:interval] || 300;
$interval_str = "&i=#{$interval}";

$period       = $options[:period] || '1M';
$period_str   = "&p=#{$period}";

$exchange_str = "&x=#{$options[:exchange]}" unless $options[:exchange].nil?

$interval_min   = $interval / 60;

$options[:quotes].each{ |quote|

  uri = "https://www.google.com/finance/getprices?q=#{quote}#{$exchange_str}#{$interval_str}#{$period_str}";
  puts "Parsing URI: '#{uri}'" if $options[:verbose];

  contents = URI.unescape(
      URI.parse(uri).read()
  )

  open_minute = close_minute = order = timezone = nil;

  epoch_scalar = 0;

  stock_data = [];

  contents.each_line { |line|
      case line
      when /^EXCHANGE=(.+)\n/
          $exchange = $1;
          puts "EXCHANGE: #{$exchange}" if $options[:verbose];

      when /^INTERVAL=(\d+)\n/
          $interval = Integer($1);
          puts "INTERVAL: #{$1}" if $options[:verbose];

      when /^MARKET_OPEN_MINUTE=(\d+)/
          open_minute = Integer($1);
          puts "OPEN: #{open_minute}" if $options[:verbose];

      when /^MARKET_CLOSE_MINUTE=(\d+)/
          close_minute = Integer($1);
          puts "CLOSE: #{close_minute}" if $options[:verbose];

      when /^COLUMNS=(.+)\n/
          order = $1.split(',');
          puts "ORDER: #{order}" if $options[:verbose];

      when /^TIMEZONE_OFFSET=(-?\d+)/
          timezone = Integer($1);
          puts "TIMEZONE: #{timezone}" if $options[:verbose];
          puts "\n" if $options[:verbose]
          puts "================\n" if $options[:debug]
      
      when /^a(.+)\n/
          line_data     = $1.split(',');
          line_data    << timezone;
          epoch_scalar  = line_data[0] = Integer(line_data[0]);
          stock_data   += [line_data];

          puts "\nEPOCH SCALAR: #{epoch_scalar}" if $options[:debug];
          puts "STOCK DATA:   #{line_data}" if $options[:debug];

      when /^(\d+,.+)/
          line_data    = $1.split(',');
          line_data   << timezone;
          current_ts   = line_data[0] = (Integer(line_data[0]) * $interval) + epoch_scalar;
          stock_data  += [line_data];

          puts "ADJUSTED TS:  #{current_ts}" if $options[:debug];
          puts "STOCK DATA:   #{line_data}" if $options[:debug];

      end
  }

  if stock_data.length < 1
      puts "No data found for '#{$exchange}:#{quote}'\n\n";
      next;
  end

  if $options[:debug] && $options[:verbose]
      stock_data.each { |tick|
          print_date = tick[0] + (tick[-1] * 60);
          puts "CAPTURED LOCAL TIME: " + DateTime.strptime("#{print_date}", '%s').strftime('%F @ %r');
      }
  end

  start_print_date = DateTime.strptime("#{stock_data[0][0] + (timezone * 60)}", '%s').strftime('%F @ %r');
  end_print_date   = DateTime.strptime("#{stock_data[-1][0] + (timezone * 60)}", '%s').strftime('%F @ %r');

  puts "Captured stock data for #{$exchange}:#{quote}\n";
  puts "  Interval: Every #{$interval} seconds (#{$interval_min} minutes)\n";
  puts "  Period:   #{start_print_date} - #{end_print_date}\n\n";

}

