class CreateTickers < ActiveRecord::Migration
  def change
    create_table :tickers do |t|
      t.text          :symbol
      t.text          :exchange
      t.float         :value
      t.datetime      :collected_at
      t.timestamps
    end
  end
end
